(function() {
"use strict";

angular
    .module("gtrackApp.components")
    .directive("gtScrollDiv", gtScrollDiv);

    function gtScrollDiv() {
        var directive = {
            restrict:"E",
            templateUrl: "/components/scrollDiv/scrollDiv.tmpl",
            scope: true,
            transclude: true,
            link: link,
            controller: controller,
            controllerAs: "vm"
        };
        return directive;
    }

    link.$inject = ["$timeout"];
    function link ($scope, element, attrs, vm, $timeout) {
        $timeout(function(){
            vm.scrollDiv = new IScroll("#scrollWrapper", {
                scrollX: true,
                scrollY: false,
                mouseWheel: true,
                scrollbars: false,
                snap: "button",
                momentum: true
            });
            vm.scrollDiv.on("scrollEnd", function () {
                // Event fires before the debounce completes, so the reported x value may report as past the endpoints.
                vm.atLastModules = (vm.scrollDiv.wrapperWidth - vm.scrollDiv.x >= vm.scrollDiv.scrollerWidth);
                vm.atFirstModules = (vm.scrollDiv.x >= 0);
                $scope.$apply();
            });
            vm.scrollDiv.refresh();
        })
    }

    controller.$inject = ["$document"];
    function controller($document) {
        var vm = this;
        vm.scrollDiv = undefined;
        vm.atLastModules = false;
        vm.atFirstModules = true;

        vm.scrollModules = function(direction) {
            var curPos = vm.scrollDiv.x;
            var wrapWidth = vm.scrollDiv.wrapperWidth;
            var scrollWidth = vm.scrollDiv.scrollerWidth;
            var buttonWidth = $document.find(".scrollDiv")[0].children[0].clientWidth;
            // +5 accounts for padding and margin, -1 accounts for final negative margin
            // When we scroll by page, it is preferable to not scroll by the full wrapWidth as this can result in the
            // leftmost element being positioned partially off the visible area. Instead, we want to scroll
            // to the point that the leftmost element is aligned with the edge of the wrapper. To do this, we calculate
            // the remainder from dividing the wrapper width by a child element's width plus padding and margins and
            // subtracting that remainder from the wrapper width. Then we subtract 1 more to account for the single
            // negative margin at the end. This gives us an offset that will shift the visible area by an exact multiple
            // of a child element, so the leftmost child will always be aligned correctly.
            // NOTE: This requires all child elements to have matching widths. If widths are dynamic, this would have to
            // be reworked to iterate through them and find the correct one to scroll to.
            var offset = wrapWidth - (wrapWidth % (buttonWidth + 5)) - 1;
            // Moving left means a positive offset, right is negative
            offset = (direction === "left") ? offset : offset * -1;
            // --To prevent debouncing on page scrolling, do a few checks--
            // If offset is positive (moving left), and current position (negative) plus offset(positive) is positive,
            // then we're gonna go past the start of the scrollDiv and debounce.
            // Instead, just scroll directly to position 0.
            if (offset > 0 && curPos + offset > 0) {
                vm.scrollDiv.scrollTo(0, 0, 500, IScroll.utils.ease.circular);
                return;
            }
            // If offset is negative (moving right), and offset (negative) plus current position (negative)
            // minus wrapper width (positive) is less than the negative of scrollWidth (initially positive),
            // then we're gonna go past the end of the scrollDiv and debounce.
            // Instead, just scroll directly to wrapWidth minus scrollWidth (which positions the right side of the last
            // child element directly at the edge of the wrapper).
            if (offset < 0 && offset + curPos - wrapWidth < -scrollWidth) {
                vm.scrollDiv.scrollTo(wrapWidth-scrollWidth, 0, 500, IScroll.utils.ease.circular);
                return;
            }
            // If neither of those cases is true, then we are at a position in the scrollDiv where a page shift will not
            // cause a debounce on either end. Therefore, go ahead and scroll by the offset.
            vm.scrollDiv.scrollBy(offset, 0, 500, IScroll.utils.ease.circular);
        };

        vm.scrollToFirstModule = function() {
            vm.scrollDiv.scrollTo(0, 0, 500, IScroll.utils.ease.circular);
        };

        vm.scrollToLastModule = function() {
            var wrapWidth = vm.scrollDiv.wrapperWidth;
            var scrollWidth = vm.scrollDiv.scrollerWidth;
            vm.scrollDiv.scrollTo(wrapWidth-scrollWidth, 0, 500, IScroll.utils.ease.circular);
        }
    }
})();
