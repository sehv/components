scrollDiv
=

## Horizontally scrollable div with swipe velocity and paging controls

Provides an element directive that creates a horizontally scrollable div.
Use of iScroll allows for swipe velocity to be included to make horizontal scrolling function the same as vertical on touchscreens.

On a desktop, scrolling can be accomplished by any of the following:

* Click and drag within the scroll area.
    * Slow drag is linear (scroll 1px for every 1px the cursor moves).
    * Fast drag with release is treated as a swipe and will have momentum.
    * As with touchscreen devices, clicking during the momentum will immediately stop it.
* Paging buttons on either side.
    * Double-arrow buttons act as endpoint scrolls and will move you to the start/end of the list.
    * Single-arrow buttons act as paging scrolls. Each click will move you one "page" (visible area) left or right.
       Offset calculations are altered slightly to try to make the leftmost visible element flush with the left edge of the visible area.

On a touchscreen device, scrolling works as usual. Drag or swipe. Paging buttons may also be used.

#### Dependencies
* iScroll
* Angularjs
* Bootstrap 3

#### [Demo](http://plnkr.co/edit/6NTvj0wE4XzSV1UMM5uX?p=preview)