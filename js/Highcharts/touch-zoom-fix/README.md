touch-zoom-fix
=

Fixes an issue in Highcharts 4.x where the Reset Zoom button doesn't appear after pinch zooming.

Initially forked from [Here](https://gist.github.com/nicholasklick/6083417)