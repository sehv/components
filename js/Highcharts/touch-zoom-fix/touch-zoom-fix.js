//Fixes an issue with pinch zooming in previous versions of this fix: 
//zooming worked, but the Reset Zoom button never appeared. Changing which tracker event is called takes care of this.
//Tested in 4.1.4 on Android

Highcharts.Chart.prototype.callbacks.push(function (chart) {
    var hasTouch = document.documentElement.ontouchstart !== undefined,
        mouseTracker = chart.pointer,
        container = chart.container,
        mouseMove;

    mouseMove = function (e) {
        if (hasTouch) {
            if (e && e.touches && e.touches.length > 1) {
                mouseTracker.onContainerTouchStart(e);
            } else {
                return;
            }
        } else {
            mouseTracker.onContainerMouseMove(e);
        }
    };

    click = function (e) {
        if (hasTouch) {
            mouseTracker.onContainerMouseMove(e);
        }
        mouseTracker.onContainerClick(e);
    }

    container.onmousemove = container.ontouchstart = container.ontouchmove = mouseMove;
    container.onclick = click;
});