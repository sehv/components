var each = Highcharts.each;
var pick = Highcharts.pick;
var UNDEFINED = Highcharts.UNDEFINED;

function defined(obj) {
    return obj !== UNDEFINED && obj !== null;
}

Highcharts.Legend.prototype.getAllItems = function () {
    var allItems = [];
    each(this.chart.series, function (series) {
        var seriesOptions = series.options;

        // Handle showInLegend. If the series is linked to another series, defaults to false.
        if (!pick(seriesOptions.showInLegend, !defined(seriesOptions.linkedTo) ? UNDEFINED : false, true)) {
            return;
        }

        // use points or series for the legend item depending on legendType
        allItems = allItems.concat(
                series.legendItems ||
                (seriesOptions.legendType === 'point' ?
                        series.data :
                        series)
        );
    });

    //NEW STUFF!!
    //HC doesn't allow plotLines to be shown in the legend. This new code fakes it.
    //We create a new series, initialize it and set the appropriate legend text and show/hide events.
    //Then we remove it from the chart series array so we don't break the CSV and calc code.
    //Then we stick the series at the front of the legend array.
    //This gives the effect of an extra series (and shows/hides the plotLine) without requiring any code
    //that works off the series array to check for the fake plotLine series.

    //Wait until the series array is populated (because of deferred loading).
    if (allItems.length > 0 && allItems[0].chart) {
        var opts = {
            name: "MAX",
            color: 'red',
            events: {
                hide: function () {
                    if( this.chart.yAxis[0].plotLinesAndBands[0] && this.chart.yAxis[0].plotLinesAndBands[0].svgElem ) {
                        this.chart.yAxis[0].plotLinesAndBands[0].svgElem.hide();
                        this.chart.yAxis[0].plotLinesAndBands[0].label.hide();
                        this.chart.xAxis[0].plotLinesAndBands[0].svgElem.hide();
                    }
                    if( this.chart.series[0].options.redLineSet ) {
                        this.chart.series[0].options.redLineSet( 'hidden' );
                    }
                    
                },
                show: function () {
                    if( this.chart.yAxis[0].plotLinesAndBands[0] && this.chart.yAxis[0].plotLinesAndBands[0].svgElem) {
                        this.chart.yAxis[0].plotLinesAndBands[0].svgElem.show();
                        this.chart.yAxis[0].plotLinesAndBands[0].label.show();
                        this.chart.xAxis[0].plotLinesAndBands[0].svgElem.show();
                    }
                    if( this.chart.series[0].options.redLineSet ) {
                        this.chart.series[0].options.redLineSet( 'visible' );
                    }
                }
            },
            visible: true,
            marker: { 
                symbol: 'square'
            }                    
        };
        var plotLegend = new Highcharts.Series();
        if( this.chart.yAxis[0].plotLinesAndBands[0] ) {
            if( this.chart.yAxis[0] &&
                this.chart.yAxis[0].plotLinesAndBands[0] &&
                this.chart.yAxis[0].plotLinesAndBands[0].svgElem &&
                this.chart.yAxis[0].plotLinesAndBands[0].svgElem.attr('visibility') === 'hidden' ){
                opts.visible = false;
            }    
        } else {
            if( this.chart.series[0].options.redLineGet ) {
                opts.visible = this.chart.series[0].options.redLineGet();
            }             
        }
        plotLegend.init(allItems[0].chart, opts);
        this.chart.series.pop();
        allItems.unshift(plotLegend);
    }

    return allItems;
};
