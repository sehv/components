PlotLine in Legend
=

By default, plotLines are not included when building the chart legend.

This is a simple override method that adds a single plotline to the legend and ties the legend record to the plotline.
This could be extended to support multiple plotlines (in the project this was originally built for, we only needed to control a single one).

Plotlines do not have the full functionality of a series, so this is mainly intended for cases where you need basic control over a chart line without polluting the series array.

##### Explanation
* getAllItems is the Highcharts method that populates the legend with series info.
* After it has completed, we created a fake series that controls the plotLine and initialize it.
* This causes it to be added to the legend array, at which point we pull it back out of the series array.
* Manually removing it from the series doesn't trigger a cleanup of the legend, so we now have a legend entry pointing to the plotLine without polluting the series array.

##### plotLine Events
* Changes to a plotLine's config (hide/show, line width/color, etc) within a HC event don't seem to trigger a redraw.
* As a result, we have to apply the changes directly to the child SVG elements and labels.